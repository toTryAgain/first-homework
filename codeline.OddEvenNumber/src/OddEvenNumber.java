import java.util.Scanner;

/**
 * Created by Igor on 03.11.2016.
 */
public class OddEvenNumber {
    public static void main(String[] args) {
        System.out.print(" Enter number :");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        if (number % 2 == 0) {
            System.out.println(" Even number ");
        } else {
            System.out.print(" Odd number");
        }
    }
}
