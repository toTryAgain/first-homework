/**
 * Created by Igor on 03.11.2016.
 */
public class AverageOfArray {
    public static void main(String[] args) {
        int m[] = {1, 1, 7};

        System.out.print(" Average is : " + avg(m));
    }

    static int avg(int[] array) {
        int sum = 0;
        int avg = 1;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            avg = sum / array.length;
        }
        return avg;
    }
}
