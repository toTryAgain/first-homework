import java.util.Scanner;

/**
 * Created by Igor on 02.11.2016.
 */
public class CalculateFactorial {

    public static void main(String[] args) {
        System.out.print(" Enter number ");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();


        if (number > 0) {
            System.out.println(" factorial is : " + factorial(number));
        } else {
            System.out.println("You are entered negative number");
        }
    }

    static int factorial(int n) {

        int result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}


